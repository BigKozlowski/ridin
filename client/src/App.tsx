import React from "react";

import Navbar from "./components/UI/Navbar/Navbar.component";
import ChatsPage from "./pages/ChatsPage/ChatsPage.component";
import GroupsPage from "./pages/GroupsPage/GroupsPage.component";
import MapPage from "./pages/MapPage/MapPage.component";
import ProfilePage from "./pages/ProfilePage/ProfilePage.component";

import "./App.css";

import { useSelector } from "react-redux";

function App() {
  const currentPage = useSelector((state: State) => state.navigation.currentPage);
  
  return (
    <div className="App">
      <Navbar
      />
      {currentPage === "map" && (
        <MapPage
        />
      )}
      {currentPage === "profile" && <ProfilePage />}
      {currentPage === "groups" && <GroupsPage/>}
      {currentPage === "chats" && <ChatsPage />}
    </div>
  );
}

export default App;
