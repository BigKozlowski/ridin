import styled from "styled-components";

export const GroupsContainer = styled.div`
  width: 100%;
  height: calc(100vh - 48px);
  margin-top: 48px;
  z-index: 0;
`