import React from "react";
import { useSelector } from "react-redux";
import Group from "../../components/Groups/Group/Group.component";
import { GroupsContainer } from "./GroupsPage.styles";

const GroupsPage = () => {
  const groups = useSelector((state: State) => state.groups.groupsList);

  return (
    <GroupsContainer>
      {groups.map(group => <Group groupName={group.name} destinations={group.destinations} locations={group.locations} key={group.id}/>)}
    </GroupsContainer>
  );
};

export default GroupsPage;
