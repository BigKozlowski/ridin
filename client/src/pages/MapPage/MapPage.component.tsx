import { LatLngTuple } from "leaflet";
import React, { Fragment, useEffect, useState } from "react";

import Map from "../../components/Map/Map.component";

const MapPage = () => {
  const [center, setCenter] = useState<LatLngTuple>([51.752566376464706, 39.188318252563484]);
  const [isCenterGotten, setIsCenterGotten] = useState(false);

  useEffect(() => {
    const geo = navigator.geolocation;
    geo.getCurrentPosition((position) => {
    }, (error) => {
      console.log(error)
    });
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        setCenter([position.coords.latitude, position.coords.longitude]);
        setIsCenterGotten(true);
      });
    }
  }, []);
  
  return (
    <Fragment>
      {isCenterGotten && <Map center={center}/>}
    </Fragment>
  );
};

export default MapPage;
