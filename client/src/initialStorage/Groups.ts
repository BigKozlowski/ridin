export const Groups: Group[] = [
  {
    name: "default",
    id: "0",
    destinations: [
      {
        id: "51.75256637646470639.188318252563484",
        position: [51.752566376464706, 39.188318252563484],
        name: "test1",
      },
      {
        id: "51.7520881657667739.20166492462159",
        position: [51.75208816576677, 39.20166492462159],
        name: "test2",
      },
      {
        id: "50.4714908513995640.17150878906251",
        position: [50.47149085139956, 40.17150878906251],
        name: "test3",
      },
    ],
    locations: [],
  },
  {
    name: "test1",
    id: "1",
    destinations: [
      {
        id: "test1",
        position: [55.752566376464706, 39.188318252563484],
        name: "test1",
      },
      {
        id: "test2",
        position: [55.75208816576677, 39.20166492462159],
        name: "test2",
      },
      {
        id: "test3",
        position: [54.47149085139956, 40.17150878906251],
        name: "test3",
      },
    ],
    locations: [],
  },
];
