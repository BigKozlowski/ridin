import { createSlice } from "@reduxjs/toolkit";

import { Groups } from "../../initialStorage/Groups";

export const groupsSlice = createSlice({
  name: "groups",
  initialState: {
    currentGroup: 0,
    groupsList: [...Groups],
    destinations: [Groups[0].destinations],
    locations: [Groups[0].locations],
    userName: "unnamed",
    selectedMarker: {type: "location", index: Groups[0].locations.findIndex(el => el.ownerName === "unnamed")},
  },
  reducers: {
    changeGroup: (state, action) => {
      state.currentGroup = state.groupsList.findIndex((el) => el.name === action.payload);
    },
    selectDestinationMarker: (state, action) => {
      state.selectedMarker = {type: "destination", index: action.payload.index};
      state.currentGroup = state.groupsList.findIndex((el) => el.name === action.payload.group);
    },
    selectLocationMarker: (state, action) => {
      state.selectedMarker = {type: "location", index: action.payload.index};
      state.currentGroup = state.groupsList.findIndex((el) => el.name === action.payload.group);
    },
    addDestination: (state, action) => {
      state.groupsList[state.currentGroup].destinations = [
        ...state.groupsList[state.currentGroup].destinations,
        {
          id: action.payload.lat.toString() + action.payload.lng.toString(),
          position: [action.payload.lat, action.payload.lng],
          name: action.payload.name,
        },
      ];
    },
    deleteDestination: (state, action) => {
      state.groupsList[state.currentGroup].destinations = [
        ...state.groupsList[state.currentGroup].destinations.filter(
          (el) => el.id !== action.payload
        ),
      ];
    },
    addLocation: (state, action) => {
      if (
        state.groupsList[state.currentGroup].locations.find(
          (el) => el.ownerName === state.userName
        )
      ) {
        state.groupsList[state.currentGroup].locations = state.groupsList[
          state.currentGroup
        ].locations.map((el) => {
          if (el.ownerName === action.payload.ownerName) {
            return { ...el, position: action.payload.position };
          }
          return el;
        });
      } else {
        state.groupsList[state.currentGroup].locations = [
          ...state.groupsList[state.currentGroup].locations,
          {
            id: action.payload.lat.toString() + action.payload.lng.toString(),
            position: [action.payload.lat, action.payload.lng],
            ownerName: state.userName,
          },
        ];
      }
    },
    setPosition: (state, action) => {
      const index = state.groupsList[state.currentGroup].destinations.findIndex(
        (el) => el.id === action.payload.id
      );
      state.groupsList[state.currentGroup].destinations[index] = {
        ...state.groupsList[state.currentGroup].destinations[index],
        position: action.payload.position,
      };
    },
  },
});

export const {
  changeGroup,
  selectDestinationMarker,
  addDestination,
  setPosition,
  deleteDestination,
  addLocation,
  selectLocationMarker
} = groupsSlice.actions;

export default groupsSlice.reducer;
