import { createSlice } from "@reduxjs/toolkit";

export const navigationSlice = createSlice({
  name: "groups",
  initialState: {
    currentPage: "map"
  },
  reducers: {
    changePage: (state, action) => {
      state.currentPage = action.payload.pageName;
    },
  },
});

export const { changePage } = navigationSlice.actions;

export default navigationSlice.reducer;
