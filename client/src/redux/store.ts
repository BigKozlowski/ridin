import { configureStore } from "@reduxjs/toolkit"

import groupsReducer from "./groups/groupsSlice";
import navigationReducer from "./navigation/navigationSlice";

export default configureStore({
  reducer: {
    groups: groupsReducer,
    navigation: navigationReducer
  },
})