import {createSlice} from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    currentUserId: "",
  },
  reducers: {
    setCurrentUser: (state, action) => {
      state.currentUserId = action.payload.userId;
    }
  }
})

export const {setCurrentUser} = userSlice.actions;

export default userSlice.reducer;