import React, { Fragment, useState } from "react";
import { useDispatch } from "react-redux";

import { changeGroup, selectDestinationMarker, selectLocationMarker } from "../../../redux/groups/groupsSlice";
import { changePage } from "../../../redux/navigation/navigationSlice";
import { BodyContainer, GroupContainer, HeaderContainer, ItemContainer, SelectButton } from "./Group.styles";

type Props = {
  groupName: string;
  destinations: Destination[];
  locations: LocationMarker[];
};

const Group = ({ groupName, destinations, locations }: Props) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const dispatch = useDispatch();

  const destinationSelectionHandler = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number) => {
    e.preventDefault();
    dispatch(selectDestinationMarker({ index: index, group: groupName }));
    dispatch(changePage({pageName: "map"}));
  };

  const locationSelectionHandler = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number) => {
    e.preventDefault();
    dispatch(selectLocationMarker({ index: index, group: groupName }));
    dispatch(changePage({pageName: "map"}));
  };

  const handleGroupChange = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, name: string) => {
    e.preventDefault();
    dispatch(changeGroup(name));
    dispatch(changePage({pageName: "map"}));
  };

  return (
    <GroupContainer>
      <HeaderContainer onClick={() => setIsExpanded((prev) => !prev)}>
        <h3>{groupName}</h3>
        <SelectButton onClick={(e) => handleGroupChange(e, groupName)}>Select group</SelectButton>
      </HeaderContainer>

      {isExpanded && (
        <BodyContainer>

          {destinations.length > 0 && (
            <Fragment>
              <h3>Destinations: </h3>
              {destinations.map((marker, index) => (
                <Fragment key={marker.id}>
                  <ItemContainer onClick={(e) => destinationSelectionHandler(e, index)}>
                    <h4>{marker.name}</h4>
                  </ItemContainer>
                </Fragment>
              ))}
            </Fragment>
          )}

          {locations.length > 0 && (
            <Fragment>
              <h3>Locations: </h3>
              {locations.map((marker, index) => (
                <Fragment key={marker.id}>
                  <ItemContainer onClick={(e) => locationSelectionHandler(e, index)}>
                    <h4>{marker.ownerName}</h4>
                  </ItemContainer>
                </Fragment>
              ))}
            </Fragment>
          )}
        </BodyContainer>
      )}
    </GroupContainer>
  );
};

export default Group;
