import styled from "styled-components";
import { BGColor, MainColor } from "../../colors.styles";

export const GroupContainer = styled.div`
  width: 80%;
  background-color: ${BGColor};
  margin: 0px auto;
  margin-top: 4rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  h3{
    margin: 0.5rem;
    color: ${MainColor}
  }
`

export const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const BodyContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  background-color: white;
  h3 {
    color: ${BGColor};
  }
`

export const ItemContainer = styled.div`
  border: 1px solid red;
  height: 2rem;
  margin: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  h4{
    margin: 0;
  }
`

export const SelectButton = styled.button`
  border: none;
  background-color: inherit;
  color: ${MainColor};
  font-size: 1rem;
`