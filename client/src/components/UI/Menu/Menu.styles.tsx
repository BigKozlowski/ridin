import styled from "styled-components";

import { MainColor, SecondAccentColor } from "../../colors.styles";

export const MenuContainer = styled.nav`
  top: 48px;
  right: 24px;
  width: 68px;
  position: absolute;
  z-index: 3;
  transition: 0.3s ease;
  display: none;
  &.active {
    display: block;
  }
  ul {
    padding: 0;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    justify-content: space-around;
  }
  li {
    list-style: none;
    padding: 12px 0;
    .menu-item {
      color: ${MainColor};
      font-size: 20px;
      text-decoration: none;
      cursor: pointer;
      &:hover {
        font-weight: bold;
      }
    }
  }
`;

export const MenuBg = styled.div`
  display: block;
  top: -40%;
  left: calc(100% + 150px);
  position: absolute;
  z-index: 1;
  width: 0;
  height: 0;
  background: radial-gradient(circle, #dc052d, ${SecondAccentColor});
  border-radius: 50%;
  transition: 0.3s ease;
  &.active-bg {
    width: 520px;
    height: 520px;
    transform: translate(-60%, -30%);
    display: initial;
  }
`;
