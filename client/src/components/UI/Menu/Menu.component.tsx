import React, { Fragment } from "react";
import { useDispatch } from "react-redux";

import { changePage } from "../../../redux/navigation/navigationSlice";

import { MenuBg, MenuContainer } from "./Menu.styles";

type Props = {isActive: boolean, toggleMenu: () => void};

const Menu = ({isActive, toggleMenu}: Props) => {
  const dispatch = useDispatch();

  const changeCurrentPage = (targetPage: string) => {
    toggleMenu();
    dispatch(changePage({pageName: targetPage}));
  }
  
  return (
    <Fragment>
      <MenuContainer className={isActive ? " active " : ""}>
        <ul>
          <li>
            <div className="menu-item" onClick={() => changeCurrentPage("profile")}>
              Profile
            </div>
          </li>
          <li>
            <div className="menu-item" onClick={() => changeCurrentPage("groups")}>
              Groups
            </div>
          </li>
          <li>
            <div className="menu-item" onClick={() => changeCurrentPage("chats")}>
              Chats
            </div>
          </li>
          <li>
            <div className="menu-item" onClick={() => changeCurrentPage("map")}>
              Map
            </div>
          </li>
        </ul>
      </MenuContainer>
      <MenuBg className={isActive ? " active-bg " : ""} />
    </Fragment>
  );
};

export default Menu;
