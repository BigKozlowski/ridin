import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { changePage } from "../../../redux/navigation/navigationSlice";
import Menu from "../Menu/Menu.component";

import { BurgerMenu, NavbarContainer, MainLogo } from "./Navbar.styles";

const Navbar = () => {
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();

  const toggleMenu = () => {
    setIsActive(prev => !prev);
  }

  const goToMain = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    e.preventDefault();
    dispatch(changePage({pageName: "map"}));
  }

  return (
    <NavbarContainer>
      <MainLogo onClick={(e) => goToMain(e)}>
        <b>Я</b>idin`
      </MainLogo>
      <BurgerMenu onClick={toggleMenu}>
        <div className="line" id="line1"></div>
        <div className="line" id="line2"></div>
        <div className="line" id="line3"></div>
      </BurgerMenu>
      <Menu isActive={isActive} toggleMenu={toggleMenu}/>
    </NavbarContainer>
  );
};

export default Navbar;
