import styled from "styled-components";

import { MainColor, AccentColor, BGColor } from "../../colors.styles";

export const NavbarContainer = styled.div`
  position: fixed;
  z-index: 1;
  padding: 8px;
  width: 100%;
  height: 32px;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${BGColor};
`;

export const MainLogo = styled.a`
  text-decoration: none;
  font-size: 1.75rem;
  color: ${MainColor};
  b {
    color: ${AccentColor};
  }
  &:hover{
      cursor: pointer;
    }
`;

export const BurgerMenu = styled.div`
  width: 32px;
  height: 32px;
  margin-right: 16px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 4;
  .line {
    height: 5px;
    width: 100%;
    background-color: ${AccentColor};
    display: block;
    border-radius: 5px;
    transition: 0.3s ease;
  }
  #line1 {
    transform: translateY(-4px);
  }
  #line2 {
    transform: translateY(0px);
  }
  #line3 {
    transform: translateY(4px);
  }
  &.active {
    .line {
      background-color: ${MainColor};
    }
    #line1 {
      transform: translateY(5px) rotateZ(-45deg);
    }
    #line2 {
      opacity: 0;
    }
    #line3 {
      transform: translateY(-5px) rotateZ(45deg);
    }
  }
`;
