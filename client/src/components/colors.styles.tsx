export const MainColor = "#fef8fd";
export const AccentColor = "#ffec78";
export const SecondAccentColor = "#ee5253";
export const BGColor = "#1e1633";
