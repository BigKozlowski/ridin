import React from "react";
import L, { LatLngTuple } from "leaflet";
import "leaflet/dist/leaflet.css";
import icon from "../../../node_modules/leaflet/dist/images/marker-icon.png";
import { MapContainer, useMapEvents } from "react-leaflet";
import { TileLayer } from "../../../node_modules/react-leaflet/lib/TileLayer";
import { useDispatch, useSelector } from "react-redux";
// import { MapContainer, TileLayer } from 'react-leaflet';

import DestinationMarker from "./DestinationMarker/DestinationMarker.component";
import { addDestination } from "../../redux/groups/groupsSlice";
import MyLocationMarker from "./LocationMarkers/MyLocationMarker/MyLocationMarker.component";

const MapEvents = () => {
  const dispatch = useDispatch();
  // eslint-disable-next-line
  const map = useMapEvents({
    click(e: { latlng: any }) {
      const name = window.prompt("Enter marker name");
      if(name === null){
        return
      }
      dispatch(
        addDestination({
          lat: e.latlng.lat,
          lng: e.latlng.lng,
          name: name,
        })
      );
    },
  });
  return null;
};

const Map = ({center}: {center: LatLngTuple}) => {
  let DefaultIcon = L.icon({
    iconUrl: icon,
  });
  const selectedMarker = useSelector((state: State) => state.groups.selectedMarker);
  const currentGroup = useSelector((state: State) => state.groups.currentGroup);

  const destinations = useSelector(
    (state: State) => state.groups.groupsList[currentGroup].destinations
  );

  L.Marker.prototype.options.icon = DefaultIcon;

  return (
    <MapContainer
      center={center}
      zoom={15}
      scrollWheelZoom={true}
      className="map-container"
      zoomControl={true}
    >
      <MapEvents />
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {destinations.map((marker, index) => (
        <DestinationMarker
          position={marker.position}
          key={marker.id}
          name={marker.name}
          openPopup={selectedMarker.index === index && selectedMarker.type === "destination"}
          id={marker.id}
        />
      ))}
      <MyLocationMarker />
    </MapContainer>
  );
};

export default Map;
