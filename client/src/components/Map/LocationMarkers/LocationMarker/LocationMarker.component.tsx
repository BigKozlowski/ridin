import { LatLngTuple } from "leaflet"
import React, { useEffect, useState } from "react"
import { Marker, Popup } from "react-leaflet"
// import { useMap } from "react-leaflet";

const LocationMarker = () => {
  const [position, setPosition] = useState<LatLngTuple|null>(null)
  // const map = useMap();

  useEffect(() => {
    const interval = setInterval(() => {
      if('geolocation' in navigator){
        navigator.geolocation.getCurrentPosition(position => {
          setPosition([position.coords.latitude, position.coords.longitude])
        })
      }
    }, 300);
    return () => clearInterval(interval);
  }, [])

  const logCoords = () => {
    console.dir(position);
  }

  return position === null ? null : (
    <Marker position={position}>
      <Popup><span onClick={logCoords}>{`You are here ${position[0]} *** ${position[1]}`}</span></Popup>
    </Marker>
  )
}

export default LocationMarker;