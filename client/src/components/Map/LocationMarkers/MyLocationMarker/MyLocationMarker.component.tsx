import { LatLngTuple } from "leaflet"
import React, { useEffect, useState } from "react"
import { Marker, Popup } from "react-leaflet"
import { useDispatch } from "react-redux";
// import { useMap } from "react-leaflet";

import {addLocation} from "../../../../redux/groups/groupsSlice";

const MyLocationMarker = () => {
  const [position, setPosition] = useState<LatLngTuple|null>(null)
  // const map = useMap();
  const dispatch = useDispatch();

  useEffect(() => {
    const interval = setInterval(() => {
      if('geolocation' in navigator){
        navigator.geolocation.getCurrentPosition(position => {
          dispatch(addLocation({lat: position.coords.latitude, lng: position.coords.longitude}))
          setPosition([position.coords.latitude, position.coords.longitude])
        })
      }
    }, 300);
    return () => clearInterval(interval);
  }, [dispatch])

  return position === null ? null : (
    <Marker position={position}>
      <Popup><span>{`You are here ${position[0]} *** ${position[1]}`}</span></Popup>
    </Marker>
  )
}

export default MyLocationMarker;