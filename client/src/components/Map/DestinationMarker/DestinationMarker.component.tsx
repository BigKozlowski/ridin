import { LatLngTuple } from "leaflet";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { Popup } from "react-leaflet";
import { Marker } from "react-leaflet";
import { useDispatch } from "react-redux";
import { useMap } from "../../../../node_modules/react-leaflet/lib/hooks";
import { deleteDestination, setPosition } from "../../../redux/groups/groupsSlice";

type Props = {
  position: LatLngTuple;
  name: String;
  openPopup: boolean;
  id: string
};

const DestinationMarker = ({
  position,
  name,
  id,
  openPopup,
}: Props) => {
  const dispatch = useDispatch();
  const [isDraggable, setIsDraggable] = useState(false);
  const map = useMap();
  const markerRef = useRef<any>(null);

  useEffect(() => {
    if (openPopup) {
      map.flyToBounds([position]);
      markerRef.current.openPopup();
    }
  }, [map, position, openPopup]);

  const eventHandlers = useMemo(
    () => ({
      dragend() {
        const marker = markerRef.current;
        if (marker != null) {
          dispatch(setPosition({id: id, position: [marker.getLatLng().lat, marker.getLatLng().lng]}))
          setIsDraggable(false);
        }
      },
    }),
    [dispatch, id],
  )

  const toggleDraggable = () => {
    markerRef.current.closePopup();
    setIsDraggable((prev) => !prev);
  };

  return (
    <Marker
      position={position}
      eventHandlers={eventHandlers}
      draggable={isDraggable}
      ref={markerRef}
    >
      <Popup minWidth={90}>
        <p>{name}</p>
        <button onClick={toggleDraggable}>{isDraggable ? "Cancel" : "Move marker"}</button>
        <button onClick={() => dispatch(deleteDestination(id))}>Delete marker</button>
      </Popup>
    </Marker>
  );
};

export default DestinationMarker;
