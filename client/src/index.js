import React from "react";
import ReactDOM from "react-dom/client";

import { Provider } from "../node_modules/react-redux/es/exports";
import store from "./redux/store";

import App from "./App";

import "./index.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
