declare module "*.png";
declare module "*.svg";

type Marker = { id: Key; lat: number; lng: number; draggable: boolean; name: string };
type Destination = { id: Key; position: LatLngTuple; name: string };
type LocationMarker = { id: Key; position: LatLngTuple; ownerName: string };
type Group = { name: string; id: string; destinations: Destination[]; locations: LocationMarker[] };

type State = {
  groups: {
    currentGroup: number;
    groupsList: Group[];
    destinations: Destination[];
    locations: LocationMarker[];
    selectedMarker: {type: "location"|"destination", index: number};
  };
  navigation: { currentPage: string };
};
