import bodyParser from 'body-parser';
import express from 'express';

class LatLng {
    lat: number;
    lng: number;
    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }
}

class User {
    login: string;
    latLng: LatLng;
    constructor(login: string, latLng: LatLng) {
        this.login = login;
        this.latLng = latLng;
    }
}

class Group {
    users: User[];
    markers: LatLng[];
    constructor() {
        this.users = [];
        this.markers = [];
    }
}

let nextGroupId = 0;
let groups = new Map<number, Group>();

const groupsRouter = express.Router();
groupsRouter.post('/', (req, res) => {
    const json = req.body.json();
    const user = new User(json.login, new LatLng(json.lat, json.lng));
    const groupId = nextGroupId++;

    const group = new Group();
    group.users.push(user);
    groups.set(groupId, group);

    res.status(201).send({'groupId': groupId});
});

const app = express();
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.text());
app.use('/groups', groupsRouter);
